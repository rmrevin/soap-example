<?php
/**
 * make.php
 * @author Revin Roman
 * @link https://rmrevin.com
 */

require __DIR__ . '/vendor/autoload.php';

$generator = new \Wsdl2PhpGenerator\Generator;
$generator->generate(
    new \Wsdl2PhpGenerator\Config(array(
        'inputFile' => 'http://mbsdevcrm13sp2.manzanagroup.ru:8145/PrivateOfficeDataService.asmx?wsdl',
        'outputDir' => __DIR__ . '/client',
    ))
);
