<?php
/**
 * test.php
 * @author Revin Roman
 * @link https://rmrevin.com
 */

require __DIR__ . '/client/autoload.php';

try {
    $service = new \PrivateOfficeDataService;
    $request = new \Authenticate('test', 'hello', '127.0.0.1', 'true');
    $response = $service->Authenticate($request);

    print_r($response);
} catch (SoapFault $e) {
    print_r($e);
}